// $(document).ready(function () {
showSeasons();
$("#season").click(function () {

});

function showSeasons() {
    $.ajax({
        type: 'GET',
        url: '/seasons',
        dataType: "json",
        success: function (response) {
            $('#seasonContainer').text("");
            response.sort().forEach(season => {
                jQuery('<div/>', {
                    class: 'sidenavItem',
                    id: season,
                    text: season,
                    onclick: "getMatches(this)"
                }).appendTo('#seasonContainer');
            });
        },
        error: function () {}
    })
}

function getMatches(event) {
    const season = event.id;
    let parent = $("#" + season).parent();
    let allChildren = (parent.children());
    for (let i = 0; i < allChildren.length; i++) {
        if (allChildren[i].id != season) {
            const childId = allChildren[i].id;
            $("#" + childId).empty().text(childId)
        }
    }
    $.ajax({
        type: 'GET',
        url: '/seasons/' + "" + season,
        dataType: "json",
        success: function (response) {
            $("#" + season).empty().text(season).addClass('active');
            count = 1;
            response.forEach(teamDetails => {
                jQuery('<div/>', {
                    class: 'sidenavSubItem',
                    id: "team" + count++,
                    teamname: teamDetails,
                    text: teamDetails,
                    onclick: "getPlayers(this)"
                }).appendTo('#' + season);
            });
        },
        error: function () {}
    })
}

function getPlayers(event) {
    const teamId = event.id;
    const teamName = $('#' + teamId).attr('teamname');
    const season = event.parentNode.id;
    $.ajax({
        type: 'GET',
        url: '/seasons/' + "" + season + "/teams/" + teamName,
        dataType: "json",
        success: function (response) {
            $("#" + teamId).empty().text(teamName).addClass('active');
            count = 1;
            response.forEach(teamDetails => {
                jQuery('<div/>', {
                    class: 'thirdlevelnav',
                    id: "players"+ count++,
                    player: teamDetails.batsmanDetail,
                    text: teamDetails.batsmanDetail,
                    onclick: "getPlayersDetails(this)"
                }).appendTo('#' + teamId);
            });
        },
        error: function () {}
    })
}

function getPlayersDetails(event) {
    const playerId = event.id;
    const playerName = $('#' + playerId).attr('player');
    // console.log("Player Id " + playerId);
    // console.log("Player Name "+playerName);
}
// })
(function ($) {
    $(document).ready(function () {
        $('#seasonContainer').click(function () {
            $('#seasonContainer div').removeClass('active');
        });
    });
})(jQuery);