# navtree-dilip
+ create sidebar with navigation tree which contains seasons,teams and players in hierarchy
+ make ajax call to fetch data from server

* Used
    + REST Architecture
    + AJAX calls
    + Mongoose
    + Express
    + EJS
    + JQuery
    + Node

To start this project do 
* npm install

and to run this project do
* npm start
