const express = require('express')
const path = require('path')
const index = require('./routes/index')
const app = express();
const mongoose = require('mongoose')
mongoose.Promise = require('bluebird')
mongoose.connect('mongodb://localhost:27017/IPLData', {
    useNewUrlParser: true
})

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use('/', index)
app.use(express.static(path.join(__dirname, 'public')))

app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function (err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app