const mongoose = require('mongoose');

let deliveriesSchema = mongoose.Schema({
    match_id : {
        type: Number,
        required: true
    },
    batting_team:{
        type:String,
        required: true
    },
    batsman: {
        type:String,
        required: true
    }
})
// console.log("Deliveries File")

var Deliveries = module.exports = mongoose.model('Deliveries', deliveriesSchema)
module.exports.getDeliveriesBattingTeam = function (callback) {
    Deliveries.distinct('batting_team', callback)
}
