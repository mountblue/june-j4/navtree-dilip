const mongoose = require('mongoose');

let matchesSchema = mongoose.Schema({
    season: {
        type: Number,
        required: true
    }
})


var Matches = module.exports = mongoose.model('Matches', matchesSchema)

module.exports.getMatchesSeason = function (callback) {
    Matches.distinct('season', callback)
}
module.exports.getMatchesPerSeason = function (year, callback) {
    Matches.distinct('team1', {
        season: year
    }, callback)
}
module.exports.getPlayerNamePerSeason = function (year, teamName, callback) {
    let getSeason = {
        $match: {
            season: Number(year),
            team1: teamName
        }
    }

    let joinDeliveryFile = {
        $lookup: {
            from: 'deliveries',
            localField: "id",
            foreignField: "match_id",
            as: "delivery"
        }
    }

    let flatMatchArray = {
        $unwind: "$delivery"
    }
    let findPlayers = {
        $group: {
            _id: {
                batsman: "$delivery.batsman",
                team: "$delivery.batting_team"
            }
        }
    }
    let findingColumn = {
        $project: {
            _id: 0,
            batsmanDetail: "$_id.batsman",
            teamDetail: "$_id.team"
        }
    }
    let matchingTeam = {
        $match: {
            teamDetail: teamName
        }
    }
    Matches.aggregate([getSeason, joinDeliveryFile, flatMatchArray, findPlayers, findingColumn, matchingTeam], callback)
}