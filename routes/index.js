const express = require('express')
const router = express.Router()
const Matches = require('../models/matches.js')
const Deliveries = require('../models/deliveries.js')


router.get('/', function (req, res, next) {
    res.render('index', {
        title: "IPL - Indian Premier League"
    })
})
router.get('/seasons', function (req, res, next) {
    Matches.getMatchesSeason(function (err, season) {
        if (err) {
            throw err
        }
        res.json(season)
    })
})

router.get('/seasons/:season', function (req, res, next) {
    const season = req.params.season;
    Matches.getMatchesPerSeason(season, function (err, matches) {
        if (err) {
            throw err
        }
        res.json(matches)
    })
})
router.get('/seasons/:season/teams/:team', function (req, res, next) {
    const season = req.params.season;
    const team = req.params.team;
    Matches.getPlayerNamePerSeason(season,team,function (err, playersName) {
        if (err) {
            throw err
        }
        res.json(playersName)
    })
})

module.exports = router